﻿using System;
using System.Windows.Threading;
using SoltaLabs.Avalon.Core.Utilities;

namespace SmartPole.Model.Sensor
{
   public sealed class TempProvider : ObservableObject<TempProvider>
   {
      private static readonly TempProvider _instance = new TempProvider();
      public static TempProvider Instance => _instance;

      private readonly TermSensor _sensor;
      readonly DispatcherTimer _timer;

      private TempProvider()
      {
         try
         {
            _temp = "16" + "°";
            _sensor = TermSensor.GetFirst();
         }
         catch (Exception)
         {
         }
         _timer = new DispatcherTimer();
         _timer.Interval = new TimeSpan(0, 0, 1);
         _timer.Tick += _timer_Tick;
      }

      private string _temp;

      public string Temp
      {
         get { return _temp; }
         private set
         {
            Set(ref _temp, value, x => x.Temp);
         }
      }

      private void _timer_Tick(object sender, EventArgs e)
      {
         if (_sensor != null)
            try
            {
               Temp = (int)_sensor.GetCelcius() + "°";
            }
            catch (Exception)
            {
               SimpleLog.Warning("Sensor read failed");
            }
      }

      public void Start()
      {
         if(!_timer.IsEnabled) _timer.Start();
      }

      public void Stop()
      {
         if(_timer.IsEnabled) _timer.Stop();
      }

   }
}
