﻿using System;
using System.Linq;
using DalSemi.OneWire.Container;
using Microsoft.Win32;
using SoltaLabs.Avalon.Core.Utilities;

namespace SmartPole.Model.Sensor
{
   public class TermSensor : IDisposable
   {
      private OneWireContainer28 _owc;

      public static TermSensor GetFirst()
      {
         try
         {
            string port = FindPort();
            DalSemi.OneWire.Adapter.PortAdapter adapter = DalSemi.OneWire.AccessProvider.GetAdapter("HOMEBREW", port);
            DalSemi.OneWire.Adapter.DeviceContainerList owSensors = adapter.GetDeviceContainers();

            return owSensors.OfType<OneWireContainer28>().Select(sensor => new TermSensor { _owc = sensor }).FirstOrDefault();

         }
         catch (Exception)
         {
            SimpleLog.Warning("No Temperature sensor found");
            return null;
         }
      }

      public double GetCelcius()
      {
         if (_owc == null) return 0;

         byte[] state = _owc.ReadDevice();
         _owc.DoTemperatureConvert(state);

         //Changes the resultion that that will be read
         _owc.SetTemperatureResolution(DalSemi.OneWire.Container.OneWireContainer28.RESOLUTION_12_BIT, state);
         //Reads the sensors id
         //         sensorIds.Add(adressAsDigitemp(owc28.AddressAsString));
         //Reads the temperature
         var t = _owc.GetTemperature(state);
         return _owc.GetTemperature(state);
      }

      public void Dispose()
      {
         _owc = null;
      }

      private static string FindPort()
      {
         try
         {
            var hardware = Registry.LocalMachine.OpenSubKey("HARDWARE");
            var deviceMap = hardware.OpenSubKey("DEVICEMAP");
            var coms = deviceMap.OpenSubKey("SERIALCOMM");
            var allNames = coms.GetValueNames();
            foreach (string name in allNames)
            {
               if (name.Contains("Prolific"))
               {
                  return coms.GetValue(name).ToString();
               }
            }
         }
         catch (Exception)
         {
         }
         return string.Empty;
      }
   }
}
