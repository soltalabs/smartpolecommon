﻿using System;
using System.Xml.Serialization;

namespace SmartPole.Model
{
    [Serializable]
    public class WebBrowserConfig
    {
        [XmlElement("StartPage")]
        public string StartPage { get; set; }

        [XmlArray("Domains")]
        [XmlArrayItem("Domain")]
        public string[] Domains { get; set; }
    }
}