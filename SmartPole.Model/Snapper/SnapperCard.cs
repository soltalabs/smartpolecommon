﻿using myfeedln;
using SoltaLabs.Avalon.Core.Helper;

namespace SmartPole.Model.Snapper
{
   internal static class SnapperCardHelper
   {
      public static string GetCardNumber(this IMyFeederLIBNFCCard theCard)
      {
         var adf = theCard.Transmit(new byte[] { 0x00, 0xa4, 0x04, 0x00, 0x07, 0xd4, 0x10, 0x00, 0x00, 0x03, 0x00, 0x01 });

         return adf.ToHex(8, 8);
      }

      public static byte[] Transmit(this IMyFeederLIBNFCCard theCard, byte[] apdu)
      {
         object obj = apdu;

         object res = theCard.Execute(ref obj);

         return (byte[])res;
      }

      public static double GetBalance(this IMyFeederLIBNFCCard theCard)
      {
         byte[] p = theCard.Transmit(new byte[] { 0, 0xB2, 1, 0x24, 0x1a });
         if (p.Length > 2)
         {
            int centBalance = ReadInt(p, 2, 4);
            return (double)centBalance/100;
         }
         return 0;
      }

      private static int ReadInt(byte[] b, int off, int len)
      {
         uint val = 0;

         while (0 != len--)
         {
            val = (val << 8) + (uint)(0xff & b[off++]);
         }

         return (int)val;
      }

      public static string ReadCardPulseInfo(this IMyFeederLIBNFCCard theCard)
      {
         var adf = theCard.Transmit(new byte[] { 0x00, 0xa4, 0x04, 0x00, 0x07, 0xd4, 0x10, 0x00, 0x00, 0x03, 0x00, 0x01 });

         return adf.Encode(0, adf.Length);
      }
   }
}
