﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Xml.Linq;
using SoltaLabs.Avalon.Core.Helper;
using SoltaLabs.Avalon.Core.Utilities;

namespace SmartPole.Model.EPay
{
   public class EPayProcessor : BackgroundWorker
   {
      private readonly EPayParam _param;

      public EPayProcessor(EPayParam param)
      {
         _param = param;
      }

      protected override void OnDoWork(DoWorkEventArgs e)
      {
         try
         {
            string xmlRequest = FillRequest(e.Argument.ToString());

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create($"{_param.Server}:{_param.Port}");
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(xmlRequest);
            request.ContentType = "text/xml; encoding='utf-8'";
            request.ContentLength = bytes.Length;
            request.Method = "POST";
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
               Stream responseStream = response.GetResponseStream();
               if (responseStream != null)
               {
                  string responseStr = new StreamReader(responseStream).ReadToEnd();
                  e.Result = ProcessResult(responseStr);
               }
            }
         }
         catch (Exception ex)
         {
            SimpleLog.Warning("EPay service failed");
            SimpleLog.Error(ex.ToString());
         }

         base.OnDoWork(e);
      }

      private string FillRequest(string xmlRequest)
      {
         xmlRequest = xmlRequest.Replace("{username}", _param.Username);
         xmlRequest = xmlRequest.Replace("{password}", _param.Password);
         xmlRequest = xmlRequest.Replace("{terminalid}", _param.TerminalId);
         xmlRequest = xmlRequest.Replace("{localtime}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
         xmlRequest = xmlRequest.Replace("{amount}", _param.Amount.ToString());
         xmlRequest = xmlRequest.Replace("{productid}", _param.ProductId);

         return xmlRequest;
      }

      private EPayResult ProcessResult(string responseStr)
      {
         EPayResult ePay = new EPayResult();
         var x = XElement.Parse(responseStr);

         var result = x.Element("RESULT");

         if (result != null && result.Value == "0")
         {
            ePay.IsSuccess = true;
            var pinCreEl = x.Element("PINCREDENTIALS");
            if (pinCreEl != null)
            {
               ePay.PinInfo = new EPayPinCredential
               {
                  Pin = pinCreEl.GetValue("PIN"),
                  Serial = pinCreEl.GetValue("SERIAL"),
                  ValidTo = pinCreEl.GetValue("VALIDTO")
               };
            }

            var receipt = x.Element("RECEIPT");
            if (receipt != null)
            {
               var merchantReceipt = receipt.Element("MERCHANT");
               if (merchantReceipt != null)
               {
                  var lines = merchantReceipt.GetValues("LINE");
                  ePay.MerchantReceipt = string.Join(Environment.NewLine, lines);
               }
               var clientReceipt = receipt.Element("CUSTOMER");
               if (clientReceipt != null)
               {
                  var lines = clientReceipt.GetValues("LINE");
                  ePay.ClientReceipt = string.Join(Environment.NewLine, lines);
               }
            }
         }

         return ePay;
      }
   }
}
