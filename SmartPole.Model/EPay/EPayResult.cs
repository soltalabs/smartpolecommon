﻿namespace SmartPole.Model.EPay
{
    public class EPayResult
    {
        public bool IsSuccess { get; set; }
        public string MerchantReceipt { get; set; }
        public string ClientReceipt { get; set; }
        public EPayPinCredential PinInfo { get; set; }
    }

    public class EPayPinCredential
    {
        public string Pin { get; set; }
        public string Serial { get; set; }
        public string ValidTo { get; set; }
    }
}
