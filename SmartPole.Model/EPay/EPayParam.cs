﻿using System.Configuration;
using System.IO;
using System.Xml.Linq;
using SoltaLabs.Avalon.Core.Helper;

namespace SmartPole.Model.EPay
{
   public class EPayParam
   {
      public string Server { get; set; }
      public string Port { get; set; }
      public string Username { get; set; }
      public string Password { get; set; }

      public string TerminalId { get; set; }
      public int Amount { get; set; }
      public string ProductId { get; set; }

      public static EPayParam CreateForAmount(int amount)
      {
         EPayParam param = new EPayParam();
         string configFile = File.ReadAllText(ConfigurationManager.AppSettings["EpayConfigFile"]);

         var config = XElement.Parse(configFile);

         param.Server = config.GetValue("Server");
         param.Port = config.GetValue("Port");
         param.Username = config.GetValue("Username");
         param.Password = config.GetValue("Password");
         param.TerminalId = config.GetValue("TerminalId");

         var pEl = config.Element("Products");
         if (pEl != null)
         {
            var keys = pEl.Elements("Product");
            foreach (var pair in keys)
            {
               if (pair.Attribute("Amount").Value == amount.ToString())
               {
                  param.ProductId = pair.Attribute("Code").Value;
                  param.Amount = amount * 100;
                  break;
               }
            }
         }

         return param;
      }
   }
}
