﻿using System;
using Smartpay.Eftpos;
using Smartpay.Eftpos.Logging;
using SoltaLabs.Avalon.Core.Utilities;

namespace SmartPole.Model.SmartPay
{
    public class SmartPayProcessor
    {
        private const string ResultKey = "TransactionResult";
        private const string OkResultValue = "OK-ACCEPTED";

        private readonly SmartPayParams _param;

        public SmartPayProcessor(SmartPayParams param)
        {
            _param = param;
        }

        /// <summary>
        /// Start payment
        /// </summary>
        /// <param name="amount">money (XX.XX)</param>
        public void Pay(double amount)
        {
            // $X0 = X000
            uint moneyAmount = (uint)amount * 100;
            CallPaymentProvider(provider => provider.Purchase(moneyAmount));
        }

        private void CallPaymentProvider(Action<IMultiMerchantSimplePaymentProvider> paymentFunction)
        {
            try
            {
                IMultiMerchantSimplePaymentProvider provider =
                    new SimplePaymentProvider(_param.Address, _param.Port, LogEventLevel.Debug);
               
                provider.Merchant = _param.Merchant;

                provider.OnLog += WriteToLog;
                provider.OnPrint += Provider_OnPrint;
                provider.OnResponse += Provider_OnResponse;

                paymentFunction(provider);
            }
            catch (Exception ex)
            {
               SimpleLog.Error(ex.ToString());
                _param.FailedCallback(ex);
            }
        }

        private void Provider_OnResponse(object sender, ResponseEventArgs e)
        {
            if (e.Response[ResultKey] == OkResultValue)
                _param.SuccessCallback(e);
            else
                _param.FailedCallback(e);
        }

        private void Provider_OnPrint(object sender, PrintEventArgs e)
        {
            //TODO: perform more print actions
        }

        private void WriteToLog(object sender, LogEventArgs e)
        {
            //TODO: perform logging here
        }
    }
}
