﻿using System;
using Smartpay.Eftpos;

namespace SmartPole.Model.SmartPay
{
    public class SmartPayParams
    {
        public string Address { get; set; }
        public int Port { get; set; }
        public int Merchant { get; set; }

        public Action<ResponseEventArgs> SuccessCallback { get; set; }
        public Action<object> FailedCallback { get; set; }
    }
}
