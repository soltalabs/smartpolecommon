﻿using System.Windows.Controls;
using SoltaLabs.Avalon.Core;
using SoltaLabs.Avalon.Core.Attributes;
using SoltaLabs.Avalon.Core.Behaviors;
using SoltaLabs.Avalon.View.Core.Adapters;
using SoltaLabs.Avalon.View.Core.Interfaces;

namespace SmartPole.View
{
   /// <summary>
   /// Interaction logic for JourneyView.xaml
   /// </summary>
   [Target(Targets.Journey)]
   public partial class JourneyView : UserControl, IHasBrowser, IContentResetable
   {
      public JourneyView()
      {
         InitializeComponent();
         _adapter = new BrowserAdapter(this, WebBrowser);

         _adapter.InitWeb();
      }

      readonly BrowserAdapter _adapter;
      public string ConfigKey => "JourneyMapConfigFile";
      BrowserAdapter IHasBrowser.Adapter => _adapter;

      public void Reset()
      {
         _adapter.InitWeb();
      }
   }
}
