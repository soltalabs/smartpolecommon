﻿using SmartPole.Model.OpenWeather;
using SmartPole.Model.Sensor;
using System;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Xml.Linq;
using SoltaLabs.Avalon.Core.Helper;
using SoltaLabs.Avalon.Core.Utilities;

namespace SmartPole.View
{
   /// <summary>
   /// Interaction logic for WeatherView.xaml
   /// </summary>
   public partial class WeatherView : UserControl
   {
      public WeatherView()
      {
         InitializeComponent();

         Loaded += WeatherView_Loaded;
         Unloaded += WeatherView_Unloaded;
      }

      private void WeatherView_Unloaded(object sender, System.Windows.RoutedEventArgs e)
      {
         if (_timer != null && _timer.IsEnabled)
            _timer.Stop();
      }

      private const string _currentUrl =
         @"http://api.openweathermap.org/data/2.5/weather?q={0}&appid=323383f81d554ace204a02d8ab49f874&mode=xml";

      private const string _forecastUrl =
         @"http://api.openweathermap.org/data/2.5/forecast?q={0}&mode=xml&appid=323383f81d554ace204a02d8ab49f874";

      private const string url = @"http://openweathermap.org/img/w/{0}.png";

      DispatcherTimer _timer;

      private void WeatherView_Loaded(object sender, System.Windows.RoutedEventArgs e)
      {
         //read weather

         txtToday.Text = DateTime.Now.ToString("dddd MMMM dd").ToUpper();

         var fileName = ConfigurationHelper.GetFileName("WeatherConfigFile");
         var content = ConfigurationHelper.ReadConfigurationContent(fileName);
         var config = XElement.Parse(content);

         //get current symbol
         try
         {
            var current = APIHelper.GetXmlReponse<current>(string.Format(_currentUrl, config.GetValue("Current")));
            BuildImg(currentImg, current.weather.icon);
         }
         catch (System.Exception)
         {

         }

         try
         {
            var forecast = APIHelper.GetXmlReponse<weatherdata>(string.Format(_forecastUrl, config.GetValue("ForeCast")));

            BuildImg(icon1, forecast.forecast[3].symbol.var);
            day1.Text = forecast.forecast[3].from.DayOfWeek.ToString().Substring(0, 3).ToUpper();

            BuildImg(icon2, forecast.forecast[11].symbol.var);
            day2.Text = forecast.forecast[11].from.DayOfWeek.ToString().Substring(0, 3).ToUpper();

            BuildImg(icon3, forecast.forecast[19].symbol.var);
            day3.Text = forecast.forecast[19].from.DayOfWeek.ToString().Substring(0, 3).ToUpper();

            BuildImg(icon4, forecast.forecast[27].symbol.var);
            day4.Text = forecast.forecast[27].from.DayOfWeek.ToString().Substring(0, 3).ToUpper();

            BuildImg(icon5, forecast.forecast[35].symbol.var);
            day5.Text = forecast.forecast[35].from.DayOfWeek.ToString().Substring(0, 3).ToUpper();
         }
         catch (System.Exception)
         {

         }

         //run temperature sensor
         try
         {
            _sensor = TermSensor.GetFirst();
         }
         catch (Exception)
         {
         }
         _timer = new DispatcherTimer();
         _timer.Interval = new TimeSpan(0, 0, 1);
         _timer.Tick += _timer_Tick;
         _timer.Start();
      }

      TermSensor _sensor;

      private void _timer_Tick(object sender, EventArgs e)
      {
         if (_sensor != null)
            try
            {
               txtTemp.Text = (int)_sensor.GetCelcius() + "°";

            }
            catch (Exception)
            {
               SimpleLog.Warning("Sensor read failed"); 
            }
        }

        public void BuildImg(Image imgage, string icon)
        {
            BitmapImage img = new BitmapImage();
            img.BeginInit();
            img.UriSource = new Uri(string.Format(url, icon), UriKind.Absolute);
            img.EndInit();

            imgage.BeginInit();
            imgage.Source = img;
            imgage.EndInit();

        }
    }
}
