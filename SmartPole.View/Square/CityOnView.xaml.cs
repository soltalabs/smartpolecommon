﻿using System.Reflection;
using System.Threading;
using System.Windows;
using SoltaLabs.Avalon.Core;
using SoltaLabs.Avalon.Core.Attributes;
using SoltaLabs.Avalon.View.Core.Adapters;
using SoltaLabs.Avalon.View.Core.Interfaces;

namespace SmartPole.View.Square
{
   /// <summary>
   /// Interaction logic for CityOnView.xaml
   /// </summary>
   [Target(Targets.Cityon)]
   public partial class CityOnView : IHasBrowser
   {
      public CityOnView()
      {
         InitializeComponent();         
         if (Application.Current.GetType().FullName.Contains("1080"))
            _adapter = new BrowserAdapter(this);
         else
            _adapter = new BrowserAdapter(this, mapViewer);
      }

      readonly BrowserAdapter _adapter;
      public string ConfigKey => "CityOnConfigFile";
      BrowserAdapter IHasBrowser.Adapter => _adapter;
   }
}
