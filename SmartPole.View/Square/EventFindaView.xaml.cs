﻿using System.Reflection;
using System.Windows;
using SoltaLabs.Avalon.Core;
using SoltaLabs.Avalon.Core.Attributes;
using SoltaLabs.Avalon.View.Core.Adapters;
using SoltaLabs.Avalon.View.Core.Interfaces;

namespace SmartPole.View.Square
{
   /// <summary>
   /// Interaction logic for EventFindaView.xaml
   /// </summary>
   [Target(Targets.EventFinda)]
   public partial class EventFindaView : IHasBrowser
   {
      public EventFindaView()
      {
         InitializeComponent();
         if(Application.Current.GetType().FullName.Contains("1080"))
            _adapter = new BrowserAdapter(this);
         else
            _adapter = new BrowserAdapter(this, mapViewer);
      }

      readonly BrowserAdapter _adapter;
      public string ConfigKey => "EventFindaConfigFile";
      BrowserAdapter IHasBrowser.Adapter => _adapter;
   }
}
