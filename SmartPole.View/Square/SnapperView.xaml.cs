﻿using System.Windows;
using System.Windows.Controls;
using SmartPole.ViewModel.Interfaces;

namespace SmartPole.View.Square
{
    /// <summary>
    /// Interaction logic for SnapperView.xaml
    /// </summary>
    public partial class SnapperView : ISnapperView
   {
      public SnapperView()
      {
         InitializeComponent();
      }

      public void ShowBlockMessage()
      {
         warningMsg.Visibility = Visibility.Visible;
      }
   }
}
