﻿using System;
using System.Linq;
using SmartPole.ViewModel.Interfaces;
using SoltaLabs.Avalon.Core;
using SoltaLabs.Avalon.Core.Attributes;
using SoltaLabs.Avalon.View.Core.Interfaces;
using SoltaLabs.Avalon.View.Core.UserControls;

namespace SmartPole.View.Square
{
   /// <summary>
   /// Interaction logic for SquareView.xaml
   /// </summary>
   public partial class SquareView : ISquareView
   {
      public SquareView()
      {
         InitializeComponent();
      }

      public ISnapperView GetSnapperView()
      {
         return CloverUc.TopRightContent as SnapperView;
      }

      public IHasBrowser GetView(Targets target)
      {
         var properties = typeof(LuckyCloverUC).GetProperties();
         foreach (var property in properties)
         {
            var value = property.GetValue(CloverUc);
            var view = value as IHasBrowser;
            if (view != null)
            {
               var customAttributes = view.GetType().GetCustomAttributes(typeof(TargetAttribute), true);

               var x = customAttributes.OfType<TargetAttribute>().FirstOrDefault();

               if (x != null && x.Target == target)
                  return view;
            }
         }

         return null;
      }
   }
}
