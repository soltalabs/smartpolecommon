﻿using SoltaLabs.Avalon.Core;
using SoltaLabs.Avalon.View.Core.Interfaces;

namespace SmartPole.ViewModel.Interfaces
{
   public interface ISquareView
   {
      ISnapperView GetSnapperView();

      IHasBrowser GetView(Targets target);
   }
}
