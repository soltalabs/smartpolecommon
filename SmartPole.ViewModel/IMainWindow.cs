﻿
using SmartPole.ViewModel.Interfaces;
using SoltaLabs.Avalon.View.Core.Interfaces;

namespace SmartPole.ViewModel
{
   public interface IMainWindow
   {
      void ToggleEmergency();
      void ToggleFeedback();

      ISquareView GetSquareView();
      IHasBrowser GetJourneyView();
   }
}
