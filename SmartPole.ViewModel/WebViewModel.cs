﻿using SoltaLabs.Avalon.Core.Utilities;
using SoltaLabs.Avalon.View.Core.Interfaces;

namespace SmartPole.ViewModel
{
   public class WebViewModel : ObservableObject<WebViewModel>, IActiveViewModel
   {
      private readonly IHasBrowser _target;

      public WebViewModel(IHasBrowser target)
      {
         _target = target;
      }

      public void OnActivate(object args)
      {
         _target.Adapter.OnActivate(args);
      }

      public void OnDeactivate(object args)
      {
         _target.Adapter.OnDeactivate(args);
      }
   }
}
