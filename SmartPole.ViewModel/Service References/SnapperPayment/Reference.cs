﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmartPole.ViewModel.SnapperPayment {
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1586.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="https://mysnapper.snapper.co.nz")]
    public partial class SyrahFault : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string faultField;
        
        private int faultCodeField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string fault {
            get {
                return this.faultField;
            }
            set {
                this.faultField = value;
                this.RaisePropertyChanged("fault");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public int faultCode {
            get {
                return this.faultCodeField;
            }
            set {
                this.faultCodeField = value;
                this.RaisePropertyChanged("faultCode");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1586.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.datacontract.org/2004/07/SyrahServer.Common")]
    public partial class CreditCardFields : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string cCExpiryMonthField;
        
        private string cCExpiryYearField;
        
        private string cCHolderNameField;
        
        private string cCNumberField;
        
        private string cCTypeField;
        
        private string cCSecurityCodeField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string CCExpiryMonth {
            get {
                return this.cCExpiryMonthField;
            }
            set {
                this.cCExpiryMonthField = value;
                this.RaisePropertyChanged("CCExpiryMonth");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string CCExpiryYear {
            get {
                return this.cCExpiryYearField;
            }
            set {
                this.cCExpiryYearField = value;
                this.RaisePropertyChanged("CCExpiryYear");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string CCHolderName {
            get {
                return this.cCHolderNameField;
            }
            set {
                this.cCHolderNameField = value;
                this.RaisePropertyChanged("CCHolderName");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string CCNumber {
            get {
                return this.cCNumberField;
            }
            set {
                this.cCNumberField = value;
                this.RaisePropertyChanged("CCNumber");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public string CCType {
            get {
                return this.cCTypeField;
            }
            set {
                this.cCTypeField = value;
                this.RaisePropertyChanged("CCType");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=5)]
        public string CCSecurityCode {
            get {
                return this.cCSecurityCodeField;
            }
            set {
                this.cCSecurityCodeField = value;
                this.RaisePropertyChanged("CCSecurityCode");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1586.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.datacontract.org/2004/07/SyrahServer.Common")]
    public partial class PaymentRedirectResult : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string sessionField;
        
        private string urlField;
        
        private CreditCardFields fieldsField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string session {
            get {
                return this.sessionField;
            }
            set {
                this.sessionField = value;
                this.RaisePropertyChanged("session");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string url {
            get {
                return this.urlField;
            }
            set {
                this.urlField = value;
                this.RaisePropertyChanged("url");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public CreditCardFields fields {
            get {
                return this.fieldsField;
            }
            set {
                this.fieldsField = value;
                this.RaisePropertyChanged("fields");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1586.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.datacontract.org/2004/07/SyrahServer.Common")]
    public partial class PaymentResult : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string messageField;
        
        private System.DateTime dateField;
        
        private bool dateFieldSpecified;
        
        private string gSTnumberField;
        
        private string companyNameField;
        
        private PaymentRedirectResult paymentRedirectField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string message {
            get {
                return this.messageField;
            }
            set {
                this.messageField = value;
                this.RaisePropertyChanged("message");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public System.DateTime date {
            get {
                return this.dateField;
            }
            set {
                this.dateField = value;
                this.RaisePropertyChanged("date");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool dateSpecified {
            get {
                return this.dateFieldSpecified;
            }
            set {
                this.dateFieldSpecified = value;
                this.RaisePropertyChanged("dateSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string GSTnumber {
            get {
                return this.gSTnumberField;
            }
            set {
                this.gSTnumberField = value;
                this.RaisePropertyChanged("GSTnumber");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string companyName {
            get {
                return this.companyNameField;
            }
            set {
                this.companyNameField = value;
                this.RaisePropertyChanged("companyName");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public PaymentRedirectResult paymentRedirect {
            get {
                return this.paymentRedirectField;
            }
            set {
                this.paymentRedirectField = value;
                this.RaisePropertyChanged("paymentRedirect");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1586.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.datacontract.org/2004/07/SyrahServer.Common")]
    public partial class PaymentInstrumentDetail : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string nameField;
        
        private bool rememberField;
        
        private bool rememberFieldSpecified;
        
        private string pinField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
                this.RaisePropertyChanged("name");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public bool remember {
            get {
                return this.rememberField;
            }
            set {
                this.rememberField = value;
                this.RaisePropertyChanged("remember");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool rememberSpecified {
            get {
                return this.rememberFieldSpecified;
            }
            set {
                this.rememberFieldSpecified = value;
                this.RaisePropertyChanged("rememberSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string pin {
            get {
                return this.pinField;
            }
            set {
                this.pinField = value;
                this.RaisePropertyChanged("pin");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="https://mysnapper.snapper.co.nz", ConfigurationName="SnapperPayment.InterfacePayment")]
    public interface InterfacePayment {
        
        [System.ServiceModel.OperationContractAttribute(Action="https://mysnapper.snapper.co.nz/Interface_Payment/makeProductPayment", ReplyAction="https://mysnapper.snapper.co.nz/InterfacePayment/authPaymentResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(SmartPole.ViewModel.SnapperPayment.SyrahFault), Action="https://mysnapper.snapper.co.nz/InterfacePayment/authPayment/Fault/SyrahFault", Name="SyrahFault")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        SmartPole.ViewModel.SnapperPayment.PaymentResult authPayment(string token, int amountCents, SmartPole.ViewModel.SnapperPayment.PaymentInstrumentDetail paymentInstrumentDetail);
        
        [System.ServiceModel.OperationContractAttribute(Action="https://mysnapper.snapper.co.nz/Interface_Payment/makeProductPayment", ReplyAction="https://mysnapper.snapper.co.nz/InterfacePayment/authPaymentResponse")]
        System.Threading.Tasks.Task<SmartPole.ViewModel.SnapperPayment.PaymentResult> authPaymentAsync(string token, int amountCents, SmartPole.ViewModel.SnapperPayment.PaymentInstrumentDetail paymentInstrumentDetail);
        
        // CODEGEN: Parameter 'hasPendingReloadResult' requires additional schema information that cannot be captured using the parameter mode. The specific attribute is 'System.Xml.Serialization.XmlArrayItemAttribute'.
        [System.ServiceModel.OperationContractAttribute(Action="https://mysnapper.snapper.co.nz/Interface_Payment/hasPendingReload", ReplyAction="https://mysnapper.snapper.co.nz/InterfacePayment/hasPendingReloadResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(SmartPole.ViewModel.SnapperPayment.SyrahFault), Action="https://mysnapper.snapper.co.nz/InterfacePayment/hasPendingReload/Fault/SyrahFaul" +
            "t", Name="SyrahFault")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        SmartPole.ViewModel.SnapperPayment.hasPendingReloadResponse hasPendingReload(SmartPole.ViewModel.SnapperPayment.hasPendingReloadRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="https://mysnapper.snapper.co.nz/Interface_Payment/hasPendingReload", ReplyAction="https://mysnapper.snapper.co.nz/InterfacePayment/hasPendingReloadResponse")]
        System.Threading.Tasks.Task<SmartPole.ViewModel.SnapperPayment.hasPendingReloadResponse> hasPendingReloadAsync(SmartPole.ViewModel.SnapperPayment.hasPendingReloadRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="https://mysnapper.snapper.co.nz/Interface_Payment/getReloadDetails", ReplyAction="https://mysnapper.snapper.co.nz/InterfacePayment/getReloadDetailsResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(SmartPole.ViewModel.SnapperPayment.SyrahFault), Action="https://mysnapper.snapper.co.nz/InterfacePayment/getReloadDetails/Fault/SyrahFaul" +
            "t", Name="SyrahFault")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        SmartPole.ViewModel.SnapperPayment.ReloadDetail getReloadDetails(string UMTC);
        
        [System.ServiceModel.OperationContractAttribute(Action="https://mysnapper.snapper.co.nz/Interface_Payment/getReloadDetails", ReplyAction="https://mysnapper.snapper.co.nz/InterfacePayment/getReloadDetailsResponse")]
        System.Threading.Tasks.Task<SmartPole.ViewModel.SnapperPayment.ReloadDetail> getReloadDetailsAsync(string UMTC);
        
        [System.ServiceModel.OperationContractAttribute(Action="https://mysnapper.snapper.co.nz/Interface_Payment/makeProductPayment", ReplyAction="https://mysnapper.snapper.co.nz/InterfacePayment/makeProductPaymentResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(SmartPole.ViewModel.SnapperPayment.SyrahFault), Action="https://mysnapper.snapper.co.nz/InterfacePayment/makeProductPayment/Fault/SyrahFa" +
            "ult", Name="SyrahFault")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        SmartPole.ViewModel.SnapperPayment.PaymentResult makeProductPayment(string token, string purseInfo, SmartPole.ViewModel.SnapperPayment.ProductDetail productDetail, SmartPole.ViewModel.SnapperPayment.PaymentInstrumentDetail paymentInstrumentDetail);
        
        [System.ServiceModel.OperationContractAttribute(Action="https://mysnapper.snapper.co.nz/Interface_Payment/makeProductPayment", ReplyAction="https://mysnapper.snapper.co.nz/InterfacePayment/makeProductPaymentResponse")]
        System.Threading.Tasks.Task<SmartPole.ViewModel.SnapperPayment.PaymentResult> makeProductPaymentAsync(string token, string purseInfo, SmartPole.ViewModel.SnapperPayment.ProductDetail productDetail, SmartPole.ViewModel.SnapperPayment.PaymentInstrumentDetail paymentInstrumentDetail);
        
        [System.ServiceModel.OperationContractAttribute(Action="https://mysnapper.snapper.co.nz/Interface_Payment/makeReloadPaymentCC", ReplyAction="https://mysnapper.snapper.co.nz/InterfacePayment/makeReloadPaymentCCResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(SmartPole.ViewModel.SnapperPayment.SyrahFault), Action="https://mysnapper.snapper.co.nz/InterfacePayment/makeReloadPaymentCC/Fault/SyrahF" +
            "ault", Name="SyrahFault")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        SmartPole.ViewModel.SnapperPayment.PaymentResult makeReloadPaymentCC(string cardNumber, SmartPole.ViewModel.SnapperPayment.CreditCardDetails creditCardDetail, int amountCents);
        
        [System.ServiceModel.OperationContractAttribute(Action="https://mysnapper.snapper.co.nz/Interface_Payment/makeReloadPaymentCC", ReplyAction="https://mysnapper.snapper.co.nz/InterfacePayment/makeReloadPaymentCCResponse")]
        System.Threading.Tasks.Task<SmartPole.ViewModel.SnapperPayment.PaymentResult> makeReloadPaymentCCAsync(string cardNumber, SmartPole.ViewModel.SnapperPayment.CreditCardDetails creditCardDetail, int amountCents);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1586.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.datacontract.org/2004/07/SyrahServer.Common")]
    public partial class ReloadDetail : object, System.ComponentModel.INotifyPropertyChanged {
        
        private int amountField;
        
        private bool amountFieldSpecified;
        
        private System.DateTime paymentDateTimeField;
        
        private bool paymentDateTimeFieldSpecified;
        
        private bool refundableField;
        
        private string sourceTypeField;
        
        private string uMTCField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public int Amount {
            get {
                return this.amountField;
            }
            set {
                this.amountField = value;
                this.RaisePropertyChanged("Amount");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AmountSpecified {
            get {
                return this.amountFieldSpecified;
            }
            set {
                this.amountFieldSpecified = value;
                this.RaisePropertyChanged("AmountSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public System.DateTime PaymentDateTime {
            get {
                return this.paymentDateTimeField;
            }
            set {
                this.paymentDateTimeField = value;
                this.RaisePropertyChanged("PaymentDateTime");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PaymentDateTimeSpecified {
            get {
                return this.paymentDateTimeFieldSpecified;
            }
            set {
                this.paymentDateTimeFieldSpecified = value;
                this.RaisePropertyChanged("PaymentDateTimeSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public bool Refundable {
            get {
                return this.refundableField;
            }
            set {
                this.refundableField = value;
                this.RaisePropertyChanged("Refundable");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string SourceType {
            get {
                return this.sourceTypeField;
            }
            set {
                this.sourceTypeField = value;
                this.RaisePropertyChanged("SourceType");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public string UMTC {
            get {
                return this.uMTCField;
            }
            set {
                this.uMTCField = value;
                this.RaisePropertyChanged("UMTC");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="hasPendingReload", WrapperNamespace="https://mysnapper.snapper.co.nz", IsWrapped=true)]
    public partial class hasPendingReloadRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="https://mysnapper.snapper.co.nz", Order=0)]
        public string cardNumber;
        
        public hasPendingReloadRequest() {
        }
        
        public hasPendingReloadRequest(string cardNumber) {
            this.cardNumber = cardNumber;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="hasPendingReloadResponse", WrapperNamespace="https://mysnapper.snapper.co.nz", IsWrapped=true)]
    public partial class hasPendingReloadResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="https://mysnapper.snapper.co.nz", Order=0)]
        [System.Xml.Serialization.XmlArrayItemAttribute("reloadDetail", Namespace="http://schemas.datacontract.org/2004/07/SyrahServer.Common", IsNullable=false)]
        public SmartPole.ViewModel.SnapperPayment.ReloadDetail[] hasPendingReloadResult;
        
        public hasPendingReloadResponse() {
        }
        
        public hasPendingReloadResponse(SmartPole.ViewModel.SnapperPayment.ReloadDetail[] hasPendingReloadResult) {
            this.hasPendingReloadResult = hasPendingReloadResult;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1586.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.datacontract.org/2004/07/SyrahServer.Common")]
    public partial class ProductDetail : object, System.ComponentModel.INotifyPropertyChanged {
        
        private int creditCentsField;
        
        private bool creditCentsFieldSpecified;
        
        private int rFPassIDField;
        
        private bool rFPassIDFieldSpecified;
        
        private int regionIDField;
        
        private bool regionIDFieldSpecified;
        
        private int authCentsField;
        
        private bool authCentsFieldSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public int creditCents {
            get {
                return this.creditCentsField;
            }
            set {
                this.creditCentsField = value;
                this.RaisePropertyChanged("creditCents");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool creditCentsSpecified {
            get {
                return this.creditCentsFieldSpecified;
            }
            set {
                this.creditCentsFieldSpecified = value;
                this.RaisePropertyChanged("creditCentsSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public int RFPassID {
            get {
                return this.rFPassIDField;
            }
            set {
                this.rFPassIDField = value;
                this.RaisePropertyChanged("RFPassID");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RFPassIDSpecified {
            get {
                return this.rFPassIDFieldSpecified;
            }
            set {
                this.rFPassIDFieldSpecified = value;
                this.RaisePropertyChanged("RFPassIDSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public int regionID {
            get {
                return this.regionIDField;
            }
            set {
                this.regionIDField = value;
                this.RaisePropertyChanged("regionID");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool regionIDSpecified {
            get {
                return this.regionIDFieldSpecified;
            }
            set {
                this.regionIDFieldSpecified = value;
                this.RaisePropertyChanged("regionIDSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public int authCents {
            get {
                return this.authCentsField;
            }
            set {
                this.authCentsField = value;
                this.RaisePropertyChanged("authCents");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool authCentsSpecified {
            get {
                return this.authCentsFieldSpecified;
            }
            set {
                this.authCentsFieldSpecified = value;
                this.RaisePropertyChanged("authCentsSpecified");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1586.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.datacontract.org/2004/07/SyrahServer.Common")]
    public partial class CreditCardDetails : object, System.ComponentModel.INotifyPropertyChanged {
        
        private int cCExpiryMonthField;
        
        private bool cCExpiryMonthFieldSpecified;
        
        private int cCExpiryYearField;
        
        private bool cCExpiryYearFieldSpecified;
        
        private string cCHolderNameField;
        
        private string cCNumberField;
        
        private string cCTypeField;
        
        private string cCSecurityCodeField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public int CCExpiryMonth {
            get {
                return this.cCExpiryMonthField;
            }
            set {
                this.cCExpiryMonthField = value;
                this.RaisePropertyChanged("CCExpiryMonth");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CCExpiryMonthSpecified {
            get {
                return this.cCExpiryMonthFieldSpecified;
            }
            set {
                this.cCExpiryMonthFieldSpecified = value;
                this.RaisePropertyChanged("CCExpiryMonthSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public int CCExpiryYear {
            get {
                return this.cCExpiryYearField;
            }
            set {
                this.cCExpiryYearField = value;
                this.RaisePropertyChanged("CCExpiryYear");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CCExpiryYearSpecified {
            get {
                return this.cCExpiryYearFieldSpecified;
            }
            set {
                this.cCExpiryYearFieldSpecified = value;
                this.RaisePropertyChanged("CCExpiryYearSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string CCHolderName {
            get {
                return this.cCHolderNameField;
            }
            set {
                this.cCHolderNameField = value;
                this.RaisePropertyChanged("CCHolderName");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string CCNumber {
            get {
                return this.cCNumberField;
            }
            set {
                this.cCNumberField = value;
                this.RaisePropertyChanged("CCNumber");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public string CCType {
            get {
                return this.cCTypeField;
            }
            set {
                this.cCTypeField = value;
                this.RaisePropertyChanged("CCType");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=5)]
        public string CCSecurityCode {
            get {
                return this.cCSecurityCodeField;
            }
            set {
                this.cCSecurityCodeField = value;
                this.RaisePropertyChanged("CCSecurityCode");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface InterfacePaymentChannel : SmartPole.ViewModel.SnapperPayment.InterfacePayment, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class InterfacePaymentClient : System.ServiceModel.ClientBase<SmartPole.ViewModel.SnapperPayment.InterfacePayment>, SmartPole.ViewModel.SnapperPayment.InterfacePayment {
        
        public InterfacePaymentClient() {
        }
        
        public InterfacePaymentClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public InterfacePaymentClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public InterfacePaymentClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public InterfacePaymentClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public SmartPole.ViewModel.SnapperPayment.PaymentResult authPayment(string token, int amountCents, SmartPole.ViewModel.SnapperPayment.PaymentInstrumentDetail paymentInstrumentDetail) {
            return base.Channel.authPayment(token, amountCents, paymentInstrumentDetail);
        }
        
        public System.Threading.Tasks.Task<SmartPole.ViewModel.SnapperPayment.PaymentResult> authPaymentAsync(string token, int amountCents, SmartPole.ViewModel.SnapperPayment.PaymentInstrumentDetail paymentInstrumentDetail) {
            return base.Channel.authPaymentAsync(token, amountCents, paymentInstrumentDetail);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        SmartPole.ViewModel.SnapperPayment.hasPendingReloadResponse SmartPole.ViewModel.SnapperPayment.InterfacePayment.hasPendingReload(SmartPole.ViewModel.SnapperPayment.hasPendingReloadRequest request) {
            return base.Channel.hasPendingReload(request);
        }
        
        public SmartPole.ViewModel.SnapperPayment.ReloadDetail[] hasPendingReload(string cardNumber) {
            SmartPole.ViewModel.SnapperPayment.hasPendingReloadRequest inValue = new SmartPole.ViewModel.SnapperPayment.hasPendingReloadRequest();
            inValue.cardNumber = cardNumber;
            SmartPole.ViewModel.SnapperPayment.hasPendingReloadResponse retVal = ((SmartPole.ViewModel.SnapperPayment.InterfacePayment)(this)).hasPendingReload(inValue);
            return retVal.hasPendingReloadResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<SmartPole.ViewModel.SnapperPayment.hasPendingReloadResponse> SmartPole.ViewModel.SnapperPayment.InterfacePayment.hasPendingReloadAsync(SmartPole.ViewModel.SnapperPayment.hasPendingReloadRequest request) {
            return base.Channel.hasPendingReloadAsync(request);
        }
        
        public System.Threading.Tasks.Task<SmartPole.ViewModel.SnapperPayment.hasPendingReloadResponse> hasPendingReloadAsync(string cardNumber) {
            SmartPole.ViewModel.SnapperPayment.hasPendingReloadRequest inValue = new SmartPole.ViewModel.SnapperPayment.hasPendingReloadRequest();
            inValue.cardNumber = cardNumber;
            return ((SmartPole.ViewModel.SnapperPayment.InterfacePayment)(this)).hasPendingReloadAsync(inValue);
        }
        
        public SmartPole.ViewModel.SnapperPayment.ReloadDetail getReloadDetails(string UMTC) {
            return base.Channel.getReloadDetails(UMTC);
        }
        
        public System.Threading.Tasks.Task<SmartPole.ViewModel.SnapperPayment.ReloadDetail> getReloadDetailsAsync(string UMTC) {
            return base.Channel.getReloadDetailsAsync(UMTC);
        }
        
        public SmartPole.ViewModel.SnapperPayment.PaymentResult makeProductPayment(string token, string purseInfo, SmartPole.ViewModel.SnapperPayment.ProductDetail productDetail, SmartPole.ViewModel.SnapperPayment.PaymentInstrumentDetail paymentInstrumentDetail) {
            return base.Channel.makeProductPayment(token, purseInfo, productDetail, paymentInstrumentDetail);
        }
        
        public System.Threading.Tasks.Task<SmartPole.ViewModel.SnapperPayment.PaymentResult> makeProductPaymentAsync(string token, string purseInfo, SmartPole.ViewModel.SnapperPayment.ProductDetail productDetail, SmartPole.ViewModel.SnapperPayment.PaymentInstrumentDetail paymentInstrumentDetail) {
            return base.Channel.makeProductPaymentAsync(token, purseInfo, productDetail, paymentInstrumentDetail);
        }
        
        public SmartPole.ViewModel.SnapperPayment.PaymentResult makeReloadPaymentCC(string cardNumber, SmartPole.ViewModel.SnapperPayment.CreditCardDetails creditCardDetail, int amountCents) {
            return base.Channel.makeReloadPaymentCC(cardNumber, creditCardDetail, amountCents);
        }
        
        public System.Threading.Tasks.Task<SmartPole.ViewModel.SnapperPayment.PaymentResult> makeReloadPaymentCCAsync(string cardNumber, SmartPole.ViewModel.SnapperPayment.CreditCardDetails creditCardDetail, int amountCents) {
            return base.Channel.makeReloadPaymentCCAsync(cardNumber, creditCardDetail, amountCents);
        }
    }
}
