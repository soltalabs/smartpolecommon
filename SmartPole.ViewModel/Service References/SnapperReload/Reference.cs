﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmartPole.ViewModel.SnapperReload {
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1586.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="https://mysnapper.snapper.co.nz")]
    public partial class SyrahFault : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string faultField;
        
        private int faultCodeField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string fault {
            get {
                return this.faultField;
            }
            set {
                this.faultField = value;
                this.RaisePropertyChanged("fault");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public int faultCode {
            get {
                return this.faultCodeField;
            }
            set {
                this.faultCodeField = value;
                this.RaisePropertyChanged("faultCode");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1586.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.datacontract.org/2004/07/SyrahServer.Common")]
    public partial class ClientVersionResponse : object, System.ComponentModel.INotifyPropertyChanged {
        
        private int codeField;
        
        private bool codeFieldSpecified;
        
        private string messageField;
        
        private int clientAppIdField;
        
        private bool clientAppIdFieldSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public int code {
            get {
                return this.codeField;
            }
            set {
                this.codeField = value;
                this.RaisePropertyChanged("code");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool codeSpecified {
            get {
                return this.codeFieldSpecified;
            }
            set {
                this.codeFieldSpecified = value;
                this.RaisePropertyChanged("codeSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string message {
            get {
                return this.messageField;
            }
            set {
                this.messageField = value;
                this.RaisePropertyChanged("message");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public int clientAppId {
            get {
                return this.clientAppIdField;
            }
            set {
                this.clientAppIdField = value;
                this.RaisePropertyChanged("clientAppId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool clientAppIdSpecified {
            get {
                return this.clientAppIdFieldSpecified;
            }
            set {
                this.clientAppIdFieldSpecified = value;
                this.RaisePropertyChanged("clientAppIdSpecified");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="https://mysnapper.snapper.co.nz", ConfigurationName="SnapperReload.InterfaceAPDU")]
    public interface InterfaceAPDU {
        
        [System.ServiceModel.OperationContractAttribute(Action="https://mysnapper.snapper.co.nz/Interface_APDU/checkClientVersion", ReplyAction="https://mysnapper.snapper.co.nz/InterfaceAPDU/checkClientVersionResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(SmartPole.ViewModel.SnapperReload.SyrahFault), Action="https://mysnapper.snapper.co.nz/InterfaceAPDU/checkClientVersion/Fault/SyrahFault" +
            "", Name="SyrahFault")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        SmartPole.ViewModel.SnapperReload.ClientVersionResponse checkClientVersion(string applicationName, string applicationVersion);
        
        [System.ServiceModel.OperationContractAttribute(Action="https://mysnapper.snapper.co.nz/Interface_APDU/checkClientVersion", ReplyAction="https://mysnapper.snapper.co.nz/InterfaceAPDU/checkClientVersionResponse")]
        System.Threading.Tasks.Task<SmartPole.ViewModel.SnapperReload.ClientVersionResponse> checkClientVersionAsync(string applicationName, string applicationVersion);
        
        [System.ServiceModel.OperationContractAttribute(Action="https://mysnapper.snapper.co.nz/Interface_APDU/beginReload", ReplyAction="https://mysnapper.snapper.co.nz/InterfaceAPDU/beginReloadResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(SmartPole.ViewModel.SnapperReload.SyrahFault), Action="https://mysnapper.snapper.co.nz/InterfaceAPDU/beginReload/Fault/SyrahFault", Name="SyrahFault")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        string beginReload(string umtc, string purseInfo, string apduinitdata);
        
        [System.ServiceModel.OperationContractAttribute(Action="https://mysnapper.snapper.co.nz/Interface_APDU/beginReload", ReplyAction="https://mysnapper.snapper.co.nz/InterfaceAPDU/beginReloadResponse")]
        System.Threading.Tasks.Task<string> beginReloadAsync(string umtc, string purseInfo, string apduinitdata);
        
        [System.ServiceModel.OperationContractAttribute(Action="https://mysnapper.snapper.co.nz/Interface_APDU/completeReload", ReplyAction="https://mysnapper.snapper.co.nz/InterfaceAPDU/completeReloadResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(SmartPole.ViewModel.SnapperReload.SyrahFault), Action="https://mysnapper.snapper.co.nz/InterfaceAPDU/completeReload/Fault/SyrahFault", Name="SyrahFault")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        bool completeReload(string umtc, string apducompletedata);
        
        [System.ServiceModel.OperationContractAttribute(Action="https://mysnapper.snapper.co.nz/Interface_APDU/completeReload", ReplyAction="https://mysnapper.snapper.co.nz/InterfaceAPDU/completeReloadResponse")]
        System.Threading.Tasks.Task<bool> completeReloadAsync(string umtc, string apducompletedata);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface InterfaceAPDUChannel : SmartPole.ViewModel.SnapperReload.InterfaceAPDU, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class InterfaceAPDUClient : System.ServiceModel.ClientBase<SmartPole.ViewModel.SnapperReload.InterfaceAPDU>, SmartPole.ViewModel.SnapperReload.InterfaceAPDU {
        
        public InterfaceAPDUClient() {
        }
        
        public InterfaceAPDUClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public InterfaceAPDUClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public InterfaceAPDUClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public InterfaceAPDUClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public SmartPole.ViewModel.SnapperReload.ClientVersionResponse checkClientVersion(string applicationName, string applicationVersion) {
            return base.Channel.checkClientVersion(applicationName, applicationVersion);
        }
        
        public System.Threading.Tasks.Task<SmartPole.ViewModel.SnapperReload.ClientVersionResponse> checkClientVersionAsync(string applicationName, string applicationVersion) {
            return base.Channel.checkClientVersionAsync(applicationName, applicationVersion);
        }
        
        public string beginReload(string umtc, string purseInfo, string apduinitdata) {
            return base.Channel.beginReload(umtc, purseInfo, apduinitdata);
        }
        
        public System.Threading.Tasks.Task<string> beginReloadAsync(string umtc, string purseInfo, string apduinitdata) {
            return base.Channel.beginReloadAsync(umtc, purseInfo, apduinitdata);
        }
        
        public bool completeReload(string umtc, string apducompletedata) {
            return base.Channel.completeReload(umtc, apducompletedata);
        }
        
        public System.Threading.Tasks.Task<bool> completeReloadAsync(string umtc, string apducompletedata) {
            return base.Channel.completeReloadAsync(umtc, apducompletedata);
        }
    }
}
