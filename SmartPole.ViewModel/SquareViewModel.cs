﻿using SmartPole.ViewModel.Interfaces;
using SoltaLabs.Avalon.Core;
using SoltaLabs.Avalon.Core.Utilities;

namespace SmartPole.ViewModel
{
    public class SquareViewModel : ObservableObject<SquareViewModel>
    {
        public SquareViewModel(ISquareView view)
        {
            EPayViewModel = new EPayViewModel();
            SnapperViewModel = new SnapperViewModel(view.GetSnapperView());
            EventFindaViewModel = new WebViewModel(view.GetView(Targets.EventFinda));
            CityOnViewModel = new WebViewModel(view.GetView(Targets.Cityon));
            EPayViewModel.MoveToStep(0);
            SnapperViewModel.MoveToStep(0);
        }

       public EPayViewModel EPayViewModel { get; }

       public SnapperViewModel SnapperViewModel { get; }

       public WebViewModel EventFindaViewModel { get; }

       public WebViewModel CityOnViewModel { get; }
    }
}
