﻿using System;
using System.Diagnostics;
using myfeedln;
using Smartpay.Eftpos;
using SmartPole.ViewModel.Utility;
using SmartPole.Model.Snapper;
using SmartPole.ViewModel.Interfaces;
using SmartPole.ViewModel.SnapperPayment;
using SoltaLabs.Avalon.Core.Helper;
using SoltaLabs.Avalon.Core.Utilities;
using SoltaLabs.Avalon.View.Core.Interfaces;

namespace SmartPole.ViewModel
{
   public class SnapperViewModel : ObservableObject<SnapperViewModel>, IActiveViewModel, IPaymentViewModel
   {
      private readonly ISnapperView _view;
      private string _currentCardNumber;
      private readonly SnapperService _service;
      private SnapperFeeder _feeder;
      private IMyFeederLIBNFCCard _card;

      #region IActiveViewModel

      public void OnActivate(object args)
      {
         if (_feeder == null)
            InitFeeder();
         _feeder?.StartMonitor();
      }

      public void OnDeactivate(object args)
      {
         ResetEverything();
         _feeder?.StopMonitor();
      }

      #endregion

      #region Feeder

      private void InitFeeder()
      {
         try
         {
            _feeder = new SnapperFeeder {CardDetected = CardDetected, CardRemoved = CardRemoved};
         }
         catch (Exception exception)
         {
            SimpleLog.Warning("Snapper Feeder is missing");
            Debug.WriteLine(exception.Message);
         }
      }

      private void CardRemoved()
      {
         //user remove card after 2nd touch
         if (Payment.ProcessStep == 3)
         {
            _currentCardNumber = string.Empty;
            Payment.MoveToStep(0);
            //there could be a chance that people remove the card too fast
         }

         IsCardPresented = false;
      }

      private void CardDetected(IMyFeederLIBNFCCard snapperCard)
      {
         _card = snapperCard;
         /*******************************************************************
          we call this method anytime a card is hold to the feeder
          when check complete, CardCheckComplete will be called
          we handle logic in that method
          cases are:
          - new flow
          - user hold card up second time for balance reload
          - user hold card up second time but a different card presented ?????
         ********************************************************************/
         StartCardCheck();
      }

      /// <summary>
      /// Flow handling function
      /// </summary>
      /// <param name="result"></param>
      private void CardCheckComplete(CardCheckResult result)
      {
         //cannot read from the card
         if (result.Status == SnapperResult.DeviceError) return;

         Balance = result.Balance.ToString("C");
         if (string.IsNullOrEmpty(_currentCardNumber))
         {
            _currentCardNumber = result.CardNumber;
            CardNumber = result.CardNumber;
            Payment.MoveToStep(1);
         }
         else
         {
            //the same card
            if (string.Compare(_currentCardNumber, result.CardNumber, StringComparison.Ordinal) == 0)
            {
               if (result.ReloadCount)
               {
                  Payment.WriteToOutput("Balance reload completed.");
                  Payment.WriteToOutput("Your new balance is: " + Balance);
               }
               else
               {
                  //perform reload but nothing pending reload found .... weird
                  Payment.WriteToOutput("Your balance is: " + Balance);
               }
               Payment.WriteToOutput("You can remove your Snapper.");
            }
            else
            {
               //user put another card
               switch (Payment.ProcessStep)
               {
                  //still at choose amount step
                  case 1:
                     _currentCardNumber = result.CardNumber;
                     CardNumber = result.CardNumber;
                     break;
                  //in the middle of payment...rare
                  case 2:
                  case 3:
                     if (result.ReloadCount)
                     {
                        Payment.WriteToOutput(@"Card {result.CardNumber} found.");
                        Payment.WriteToOutput("Your balance is: " + Balance);
                     }
                     break;
               }
            }
         }

         IsCardPresented = true;
      }

      #endregion

      public SnapperViewModel(ISnapperView view)
      {
         _view = view;
         _payment = new PaymentAdapter(this);
         _service = new SnapperService();
         IsCardPresented = false;
         InitFeeder();
      }

      #region Properties

      public bool IsCardPresented
      {
         get { return !string.IsNullOrEmpty(_currentCardNumber); }
         private set { NotifyChange(CreateArgs(x => x.IsCardPresented)); }
      }

      private string _cardNumber;

      public string CardNumber
      {
         get { return _cardNumber; }
         set { Set(ref _cardNumber, value, x => x.CardNumber); }
      }

      private string _balance;

      public string Balance
      {
         get { return _balance; }
         set { Set(ref _balance, value, x => x.Balance); }
      }

      #endregion

      #region IPaymentViewModel

      private PaymentAdapter _payment;

      public PaymentAdapter Payment
      {
         get { return _payment; }
         set { Set(ref _payment, value, x => x.Payment); }
      }

      public void MoveToStep(int step)
      {
         switch (step)
         {
            case 0:
               _currentCardNumber = string.Empty;
               CardNumber = string.Empty;
               Balance = string.Empty;
               IsCardPresented = false;
               break;
         }
      }

      public void SmartPaySuccess(ResponseEventArgs obj)
      {
         DoPaymentTopUp(Payment.AmountSelectionViewModel.Amount*100);
      }

      #endregion

      private void DoPaymentTopUp(int amount)
      {
         try
         {
            var dom = SecuredModel.GetCreditCardXml();
            CreditCardDetails cc = new CreditCardDetails
            {
               CCType = dom.GetValue("Type"),
               CCNumber = dom.GetValue("Number"),
               CCExpiryMonth = int.Parse(dom.GetValue("ExpiredMonth")),
               CCExpiryYear = int.Parse(dom.GetValue("ExpiredYear")),
               CCSecurityCode = dom.GetValue("CVV"),
               CCExpiryYearSpecified = true,
               CCExpiryMonthSpecified = true
            };

            DispatcherHelper.InvokeDelegate(new Action(() =>
            {
               switch (_service.MakePayment(_currentCardNumber, amount, cc))
               {
                  case SnapperResult.Ok:
                     Payment.WriteToOutput("Payment transaction completed.");
                     Payment.WriteToOutput("Hold your snapper card to the reader to RELOAD.");
                     Payment.MoveToStep(3);
                     break;
                  case SnapperResult.ThisTransactionError:
                     Payment.WriteToOutput("Transaction error. Connection broke.");
                     Payment.WriteToOutput("Please try again.");
                     break;
               }
            }));
         }
         catch (Exception)
         {
            Payment.WriteToOutput("Connection Error.");
            Payment.WriteToOutput("Please try again.");
         }
      }

      private void StartCardCheck()
      {
         CardCheckResult result = new CardCheckResult();
         try
         {
            result.CardNumber = _card.GetCardNumber();
            result.Balance = _card.GetBalance();
         }
         catch (Exception)
         {
            result.Status = SnapperResult.DeviceError;
         }

         try
         {
            var info = _service.CheckCardInformation(_card);

            //this card is Blocked
            if (info.hotlist)
            {
               _view.ShowBlockMessage();
               return;
            }
         }
         catch (Exception)
         {
            // ignored
         }

         SnapperReloader process = new SnapperReloader(_card);
         DispatcherHelper.InvokeDelegate(new Action(() =>
         {
            try
            {
               if (process.Reload(_service) == SnapperResult.Ok)
                  result.ReloadCount = true;
               result.Balance = _card.GetBalance();
            }
            catch (Exception ex)
            {
               result.Status = SnapperResult.ThisTransactionError;
               SimpleLog.Warning("Cannot reload balance");
               SimpleLog.Error(ex.ToString());
            }

            CardCheckComplete(result);
         }));
      }

      private void ResetEverything()
      {
         _currentCardNumber = string.Empty;
         CardNumber = string.Empty;
         Balance = string.Empty;
         IsCardPresented = false;
         Payment.MoveToStep(0);
      }

      internal class CardCheckResult
      {
         public string CardNumber { get; set; }
         public double Balance { get; set; }
         public bool ReloadCount { get; set; }

         public SnapperResult Status { get; set; }
      }
   }
}
