﻿using System;
using System.Linq;
using myfeedln;
using SmartPole.ViewModel.SnapperPayment;
using SmartPole.Model.Snapper;
using SoltaLabs.Avalon.Core.Helper;
using SoltaLabs.Avalon.Core.Utilities;

namespace SmartPole.ViewModel.Utility
{
   public enum SnapperResult
   {
      Ok = 0,
      NoPendingTransaction,
      LastTransactionError,
      ThisTransactionError,
      DeviceError = 4,
   }

   public class SnapperReloader
   {
      private readonly IMyFeederLIBNFCCard _card;
      private string _cardNumber;
      private byte[] _adf;

      public SnapperReloader(IMyFeederLIBNFCCard card,  string cardNumber = "")
      {
         _card = card;
         _cardNumber = cardNumber;
      }

      public SnapperResult Reload(SnapperService service)
      {
         _adf = _card.Transmit(new byte[] { 0x00, 0xa4, 0x04, 0x00, 0x07, 0xd4, 0x10, 0x00, 0x00, 0x03, 0x00, 0x01 });
         _cardNumber = _adf.ToHex(8, 8);
         try
         {
            ReloadDetail[] list = service.GetPendingReload(_cardNumber);
            if (list.Length == 0)
               return SnapperResult.NoPendingTransaction;

            if (!list.Aggregate(false, (current, detail) => current | PerformSingleReload(service, detail.Amount, detail.UMTC)))
               return SnapperResult.LastTransactionError;

            return SnapperResult.Ok;
         }
         catch (Exception e)
         {
            SimpleLog.Error(e.ToString());

            return SnapperResult.DeviceError;
         }
      }

      private bool PerformSingleReload(SnapperService service, int amount, string umtc)
      {
         byte[] apdu = _card.Transmit(new byte[]{0x90,0x40,0,0,4,
                (byte)(amount>>24),
                (byte)(amount>>16),
                (byte)(amount>>8),
                (byte)amount});

         string res = service.BeginReload(umtc, _adf, apdu);

         apdu = _card.Transmit(res.Decode());

         service.CompleteReload(umtc, apdu);

         return true;
      }
   }
}