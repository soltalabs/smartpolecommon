﻿using System;
using System.Windows.Threading;
using myfeedln;

namespace SmartPole.ViewModel.Utility
{
   public class SnapperFeeder
   {
      private readonly IMyFeederLIBNFCReader _reader;
      public Action<IMyFeederLIBNFCCard> CardDetected { get; set; }
      public Action CardRemoved { get; set; }

      private readonly DispatcherTimer _monitor;
      private bool _hasCard;

      public SnapperFeeder()
      {
         CMyFeederLIBNFCReader reader = new CMyFeederLIBNFCReader();
         _reader = (IMyFeederLIBNFCReader)reader;

         _monitor = new DispatcherTimer();
         _monitor.Tick += delegate { CheckForCard(); };
         _monitor.Interval = new TimeSpan(0, 0, 0, 2);
      }

      public void StartMonitor()
      {
         _hasCard = false;
         _monitor.Start();
      }

      public void StopMonitor()
      {
         _monitor.Stop();
      }

      public void CheckForCard()
      {
         try
         {
            IMyFeederLIBNFCCard card = _reader.Connect("*");
            //has card
            if (!_hasCard && CardDetected != null)
            {
               _hasCard = true;
               CardDetected(card);
            }
         }
         catch (Exception)
         {
            if (_hasCard && CardRemoved != null)
            {
               _hasCard = false;
               CardRemoved();
            }
         }
      }
   }
}
