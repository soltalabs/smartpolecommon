﻿using System;
using myfeedln;
using SmartPole.Model.Snapper;
using SmartPole.ViewModel.SnapperInformation;
using SmartPole.ViewModel.SnapperPayment;
using SmartPole.ViewModel.SnapperReload;
using SoltaLabs.Avalon.Core.Helper;
using SoltaLabs.Avalon.Core.Utilities;

namespace SmartPole.ViewModel.Utility
{
   public class SnapperService
   {
      private readonly InterfacePaymentClient _paymentSvc;
      private readonly InterfaceAPDUClient _reloadSvc;
      private readonly InterfaceCardInformationClient _informationSvc;

      private const string MarketData = "MDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDA5MDAw";


      public SnapperService()
      {
         _paymentSvc = new InterfacePaymentClient();
         _reloadSvc = new InterfaceAPDUClient();
         _informationSvc = new InterfaceCardInformationClient();
      }

      public SnapperResult MakePayment(string cardNumber, int amountInCent, CreditCardDetails cc)
      {
         try
         {
            PaymentResult result = _paymentSvc.makeReloadPaymentCC(cardNumber, cc, amountInCent);

            return SnapperResult.Ok;
         }
         catch (Exception e)
         {
            SimpleLog.Error(e.ToString());
            return SnapperResult.ThisTransactionError;
         }
      }

      public string BeginReload(string umtc, byte[] adf, byte[] apdu)
      {
         return _reloadSvc.beginReload(umtc,
            adf.Encode(0, adf.Length),
            apdu.Encode(0, apdu.Length));
      }

      public bool CompleteReload(string umtc, byte[] apdu)
      {
         return _reloadSvc.completeReload(umtc, apdu.Encode(0, apdu.Length));
      }

      public ReloadDetail[] GetPendingReload(string cardNumber)
      {
         return _paymentSvc.hasPendingReload(cardNumber);
      }

      public CardInformation CheckCardInformation(IMyFeederLIBNFCCard theCard)
      {
         string pulseInfo = theCard.ReadCardPulseInfo();
         return _informationSvc.describeCard(pulseInfo,
            new[]
            {
               new MarketData
               {
                  data = MarketData,
                  distribution = 0,
                  distributionSpecified = true
               }
            });
      }
   }
}
