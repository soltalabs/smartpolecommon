﻿using System;
using System.Linq;
using System.Threading;
using System.Windows.Threading;
using SoltaLabs;
using SoltaLabs.ATBusModel;
using SoltaLabs.Avalon.Core.Helper;
using SoltaLabs.Avalon.Core.Utilities;

namespace SmartPole.ViewModel
{
   public class BusStopViewModel : ObservableObject<BusStopViewModel>
   {
      private const string BusApi =
         @"https://api.at.govt.nz/v1/public-restricted/formattedDepartures?stopCodes={0}&api_key={1}";

      private const int BusStopNo = 7035;
      private static readonly Guid ApiKey = new Guid("6772487e-84a0-4bc5-9966-84fe18583728");

      private DispatcherTimer _clockThread;

      private bool _stop;
      private Thread _apiThread;

      private int _stopNo;

      public BusStopViewModel()
      {
         _lines = 7;
         try
         {
            StopNo = int.Parse(ConfigurationHelper.GetFileName("DefaultBusStop"));
         }
         catch (Exception)
         {
            SimpleLog.Warning("Config bus stop no not found");
            StopNo = BusStopNo;
         }
      }

      public BusStopViewModel(int line)
         : this()
      {
         _lines = line;
      }

      private readonly int _lines;

      public int StopNo
      {
         get { return _stopNo; }
         set
         {
            if (_stopNo != value)
            {
               _stopNo = value;
               NotifyChange(CreateArgs(x => x.StopNo));
            }
         }
      }
      private string _time;

      public string Time
      {
         get { return _time; }
         set
         {
            if (_time != value)
            {
               _time = value;
               NotifyChange(CreateArgs(x => x.Time));
            }
         }
      }

      private Response _atBusInfo;
      public Response AtBusInfo
      {
         get { return _atBusInfo; }
         set
         {
            if (_atBusInfo != value)
            {
               _atBusInfo = value;
               NotifyChange(CreateArgs(x => x.AtBusInfo));
            }
         }
      }

      public void Start()
      {
         _clockThread = new DispatcherTimer { Interval = new TimeSpan(0, 0, 0, 1) };
         _clockThread.Tick += _clockThread_Tick;
         _clockThread.Start();

         _apiThread = new Thread(ReadAtApi);
         _apiThread.Start();
      }

      private void ReadAtApi()
      {
         while (!_stop)
         {
            try
            {
               string uri = string.Format(BusApi, BusStopNo, ApiKey);
               var busInfo = APIHelper.GetJsonReponse<BusInfo>(uri, null);
               if (busInfo.response.departures.Length > _lines)
                  busInfo.response.departures = busInfo.response.departures.Take(_lines).ToArray();

               DispatcherHelper.InvokeDelegate((Action)(() =>
               {
                  AtBusInfo = busInfo.response;
               }));
            }
            catch (Exception ex)
            {
            }

            Thread.Sleep(5000);
         }
      }

      private void _clockThread_Tick(object sender, EventArgs e)
      {
         DateTime now = DateTime.Now;
         Time = $"{now.Hour}:{now.Minute:00}:{now.Second:00}";
      }

      public void Close()
      {
         _stop = true;
         if (_clockThread != null && _clockThread.IsEnabled)
            _clockThread.Stop();
      }
   }
}
