﻿using System;
using SoltaLabs.Avalon.Core.Utilities;

namespace SmartPole.ViewModel
{
   public class AmountSelectionViewModel : ObservableObject<AmountSelectionViewModel>
   {
      private bool _isSelectable;
      public bool IsSelectable
      {
         get { return _isSelectable; }
         set
         {
            Set(ref _isSelectable, value, x => x.IsSelectable);
         }
      }

      private int _amount;

      /// <summary>
      /// int dollar $10 => 10;
      /// </summary>
      public int Amount
      {
         get { return _amount; }
         set
         {
            Set(ref _amount, value, x => x.Amount);
            if (_amount > 0)
               AmountSelected?.Invoke();
         }
      }

      public Action AmountSelected { get; set; }
   }
}
