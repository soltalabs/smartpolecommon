﻿using System;
using System.Windows.Input;
using Smartpay.Eftpos;
using SmartPole.Model.SmartPay;
using System.Configuration;
using SoltaLabs.Avalon.Core.Behaviors;
using SoltaLabs.Avalon.Core.Utilities;

namespace SmartPole.ViewModel
{
   public interface IPaymentViewModel
   {
      void MoveToStep(int step);
      void SmartPaySuccess(ResponseEventArgs obj);
   }

   public class PaymentAdapter : ObservableObject<PaymentAdapter>
   {
      private readonly string _smartPayIp;
      private readonly int _smartPayPort;

      private readonly IPaymentViewModel _viewModel;

      public PaymentAdapter(IPaymentViewModel viewModel)
      {
         _smartPayIp = ConfigurationManager.AppSettings["SmartPayIp"];
         _smartPayPort = int.Parse(ConfigurationManager.AppSettings["SmartPayPort"]);

         _viewModel = viewModel;
         _amountSelectionViewModel = new AmountSelectionViewModel();
      }

      private AmountSelectionViewModel _amountSelectionViewModel;
      public AmountSelectionViewModel AmountSelectionViewModel
      {
         get { return _amountSelectionViewModel; }
         set
         {
            Set(ref _amountSelectionViewModel, value, x => x.AmountSelectionViewModel);
         }
      }

      private int _processStep;
      public int ProcessStep
      {
         get { return _processStep; }
         set
         {
            Set(ref _processStep, value, x => x.ProcessStep);
         }
      }

      private bool _isShowPaymentProcess;
      public bool IsShowPaymentProcess
      {
         get { return _isShowPaymentProcess; }
         set
         {
            Set(ref _isShowPaymentProcess, value, x => x.IsShowPaymentProcess);
         }
      }

      private string _paymentStatus;
      public string PaymentStatus
      {
         get { return _paymentStatus; }
         set
         {
            Set(ref _paymentStatus, value, x => x.PaymentStatus);
         }
      }

      private string _eMail;
      public string Email
      {
         get { return _eMail; }
         set { Set(ref _eMail, value, x => x.Email); }
      }

      internal void MoveToStep(int step)
      {
         ProcessStep = step;
         switch (step)
         {
            case 0:
               AmountSelectionViewModel.Amount = 0;
               AmountSelectionViewModel.IsSelectable = true;
               IsShowPaymentProcess = false;
               PaymentStatus = string.Empty;
               Email = string.Empty;
               break;
            case 1:
               IsShowPaymentProcess = false;
               AmountSelectionViewModel.IsSelectable = true;
               PaymentStatus = string.Empty;
               break;
            case 2:
               AmountSelectionViewModel.IsSelectable = false;
               IsShowPaymentProcess = true;
               break;
            case 3:
               break;
         }
         _viewModel.MoveToStep(step);
      }

      public ICommand ResetCommand
      {
         get { return new DelegateCommand(o => MoveToStep(0)); }
      }

      public ICommand BackCommand
      {
         get { return new DelegateCommand(o => MoveToStep(1)); }
      }

      public ICommand PayCommand => new DelegateCommand(StartPayment);

      private void StartPayment(object obj)
      {
         if (AmountSelectionViewModel.Amount == 0) return;
         MoveToStep(2);
         WriteToOutput("Tap your payment card to the payment icon");

         SmartPayParams smartPayParams = new SmartPayParams
         {
            Address = _smartPayIp,
            Merchant = 1,
            Port = _smartPayPort,
            SuccessCallback = SmartPaySuccess,
            FailedCallback = SmartFailed
         };

         SmartPayProcessor payProcessor = new SmartPayProcessor(smartPayParams);
#if DEBUG
         SmartPaySuccess(null);
#else
         payProcessor.Pay(AmountSelectionViewModel.Amount);
#endif
        }

      private void SmartFailed(object obj)
      {
         WriteToOutput("Transaction denied");
         MoveToStep(1);
      }

      private void SmartPaySuccess(ResponseEventArgs obj)
      {
         _viewModel.SmartPaySuccess(obj);
      }

      internal void WriteToOutput(string message)
      {
         PaymentStatus += Environment.NewLine + message;
      }
   }
}
