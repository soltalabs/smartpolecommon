﻿using System;
using System.ComponentModel;
using SmartPole.Model.EPay;
using Smartpay.Eftpos;
using SmartPole.ViewModel.Properties;
using SoltaLabs.Avalon.Core.Helper;
using SoltaLabs.Avalon.Core.Utilities;
using SoltaLabs.Avalon.View.Core.Interfaces;

namespace SmartPole.ViewModel
{
   public class EPayViewModel : ObservableObject<EPayViewModel>, IPaymentViewModel, IActiveViewModel
   {
      #region IPaymentViewModel

      private PaymentAdapter _payment;
      public PaymentAdapter Payment
      {
         get { return _payment; }
         set
         {
            Set(ref _payment, value, x => x.Payment);
         }
      }

      public void MoveToStep(int step)
      {
         switch (step)
         {
            case 0:
               Payment.AmountSelectionViewModel.IsSelectable = true;
               Email = string.Empty;
               break;
            case 1:
               break;
            case 2:
               break;
            case 3:
               break;
         }
      }

      public void SmartPaySuccess(ResponseEventArgs obj)
      {
         Payment.WriteToOutput(@"Purchasing ${Payment.AmountSelectionViewModel.Amount} voucher......");

         EPayParam param = EPayParam.CreateForAmount(Payment.AmountSelectionViewModel.Amount);
         EPayProcessor payProcessor = new EPayProcessor(param);
         payProcessor.ProgressChanged += PayProcessorOnProgressChanged;
         payProcessor.RunWorkerCompleted += PayProcessorOnRunWorkerCompleted;

         payProcessor.RunWorkerAsync(Resources.EPayRequest);
      }

      #endregion

      public EPayViewModel()
      {
         _payment = new PaymentAdapter(this);
      }

      private string _eMail;
      public string Email
      {
         get { return _eMail; }
         set { Set(ref _eMail, value, x => x.Email); }
      }

      private void PayProcessorOnProgressChanged(object sender, ProgressChangedEventArgs progressChangedEventArgs)
      {
         DispatcherHelper.InvokeDelegate(new Action(() =>
         {
            Payment.WriteToOutput(progressChangedEventArgs.UserState.ToString());
         }));
      }

      private void PayProcessorOnRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs runWorkerCompletedEventArgs)
      {
         DispatcherHelper.InvokeDelegate(new Action(() =>
         {
            EPayResult result = runWorkerCompletedEventArgs.Result as EPayResult;
            if (result != null)
            {
               if (result.IsSuccess)
               {
                  MoveToStep(3);
                  //Payment.WriteToOutput("****************************");
                  //Payment.WriteToOutput("Recharge Number:");
                  //Payment.WriteToOutput(result.PinInfo.Pin);
                  //Payment.WriteToOutput("Expries:");
                  //Payment.WriteToOutput(result.PinInfo.ValidTo);
                  //Payment.WriteToOutput("Amount:");
                  //Payment.WriteToOutput(Payment.AmountSelectionViewModel.Amount.ToString("C"));
                  //Payment.WriteToOutput("Serial Number:");
                  //Payment.WriteToOutput(result.PinInfo.Serial);
                  //Payment.WriteToOutput("****************************");
                  Payment.WriteToOutput(result.ClientReceipt);
               }
               else
               {
                     //should refund
                     Payment.WriteToOutput("Voucher purchase failed... start refunding now");
                     ///TODO testing only, not refund yet
                     MoveToStep(2);
               }
            }
         }));
      }

      public void OnActivate(object args)
      {
         MoveToStep(0);
      }

      public void OnDeactivate(object args)
      {
         Email = string.Empty;
      }
   }
}
